# irit-latex-pandoc-template
This project contains my [Pandoc template](https://pandoc.org/MANUAL.html#templates) to generate [IRIT](https://www.irit.fr/en/home/)-looking PDFs from a [Pandoc Markdown](https://pandoc.org/MANUAL.html#pandocs-markdown) content.

## Usage
1. Clone this repo
2. (Install [Pandoc](https://pandoc.org/) and a [LaTeX suite](https://en.wikipedia.org/wiki/TeX_Live))
3. `pandoc -o example.pdf example.md --template latex.template`

## Example
- Markdown: [example.md](./example.md)
- Generated PDF: [example.pdf](./example.pdf)
